# lfnw/live

LinuxFest Northwest's live streaming site

Powered by:

* [Jekyll](https://jekyllrb.com/)
* [Bootstrap](https://getbootstrap.com/docs/4.1/)
* [FontAwesome Free](https://fontawesome.com/free)
