---
layout: post
title:  "Live stream for Sunday, April 28, 2019"
date:   2019-04-24 12:00:00 -0700
tags: []
permalink: /sunday
---
<!--more-->
<div class="embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" src="https://www.youtube-nocookie.com/embed/gcCMKKArDMc" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
